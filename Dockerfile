FROM selenium/standalone-chrome:latest
RUN sudo apt update \
    && sudo apt install -y python3-pip \
    && sudo rm -rf /var/lib/apt/lists/*
COPY requirements.txt .
RUN pip3 --no-cache-dir install -r requirements.txt \
    && sudo rm requirements.txt \
    && sudo ln -s /home/seluser/.local/bin/pytest /usr/bin/pytest \
    && sudo ln -s /home/seluser/.local/bin/sbase /usr/bin/sbase \
    && sbase get chromedriver
CMD [ "/bin/bash" ]