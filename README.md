# Docker container for running headless selenium base chrome tests

## Build
docker build -t python-selenium-tester .

## Usage
You can run container against your own selenium base test files using pytest in pipelines or manually.